<?php
    $productError = '';
    $priceError = '';
    if (isset($_POST['product'])) {
        $productName = $_POST['product'];
        if (empty($productName)) {
            $productError = 'Product moet een naam hebben!';
        } else {
            $productPattern = '/^[a-zA-Zéç ]*$/';
            if (!preg_match($productPattern, $productName)) {
                $productError = 'Naam bestaat uit karakters alleen!';
            }
        }

    }
    if (isset($_POST['price'])) {
        $price = $_POST['price'];
        if (empty($price)) {
            $productError = 'Product moet een prijs hebben!';
        } else {    
            $pricePattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
            if (!preg_match($pricePattern, $price)) {
                $priceError = 'Typ een geldig eurogetal in! Bv. 99.00';
            }
        }
    }
    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
        switch ($gender) {
            case 0 :
                $genderText = 'man';
                break;
            case 1 :
                $genderText = 'vrouw';
                break;        
            case 2 :
                $genderText = 'anders';
                break;
            default :
                $genderText = 'onbekend';
        }
    }
     if (isset($_POST['category'])) {
        $category = $_POST['category'];
        switch ($category) {
            case 0 :
                $categoryText = 'Huishoudgerief';
                break;
            case 1 :
                $categoryText = 'Electronica';
                break;        
            case 2 :
                $categoryText = 'Snoep';
                break;
            case 3 :
                $categoryText = 'Boeken';
                break;
            default :
                $categoryText = 'Onbekend';
        }
    }
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Formulier validatie</title>
</head>
<body>
        <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
            <label for="product"></label>
            <input id="product" name="product" type="text" required>
            <span><?php echo $productError;?></span>
            <br>
            <label for="price"></label>
            <input id="price" name="price" type="text" required pattern="^[0-9]+(\.[0-9]{1,2})?$">
            <span><?php echo $priceError;?></span>
            <br>
            <div>
                <label for="man">Man</label>
                <input type="radio" id="man" name="gender" value="0">
                <label for="woman">Vrouw</label>
                <input type="radio" id="woman" name="gender" value="1">
                <label for="other">Anders</label>
                <input type="radio" id="other" name="gender" value="2">
            </div>
            <br>
            <div>
                <label for="category">Categorie</label>
                <select name="category" id="category">
                    <option value="0">Huishoudgerief</option>
                    <option value="1">Electronica</option>
                    <option value="2">Snoep</option>
                    <option value="3">Boeken</option>
                </select>
           </div>
            <button type=submit>Verzenden</button>
        </form>
        <div>
            <?php echo "Je bent een {$genderText}";?>
            <br>
            <?php echo "Je {$productName}";?>
            <?php echo "kost {$price} Euro";?>
            <br>
            <?php echo "Te vinden bij {$categoryText}";?>
        </div>
</body>
</html>

<!-- 
^                   # Start of string.
[0-9]+              # Must have one or more numbers.
(                   # Begin optional group.
    \.              # The decimal point, . must be escaped, 
                    # or it is treated as "any character".
    [0-9]{1,2}      # One or two numbers.
)?                  # End group, signify it's optional with ?
$                   # End of string.
-->