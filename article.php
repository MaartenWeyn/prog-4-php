<?php
use \ModernWays\Dialog\Model\NoticeBoard as NoticeBoard;
use \ModernWays\Mvc\Request as Request;
use \ModernWays\Mvc\Route as Route;
use \ModernWays\Mvc\RouteConfig as RouteConfig;

include __DIR__ . '/vendor/autoload.php';
$appState = new NoticeBoard();
$request = new Request('/home/editing');
$route = new Route($appState, $request->uc());
// de namespace waarin de klassen staan van mijn app/project
// in de psr4 autoload moet ik dan het pad opgeven waar de klassen
// van die namespace staan
//
// de volgende methode maakt een instantie van de klasse Home en voert de methode
// index van die klasse uit op voorwaarde dat er geen andere route
// wordt meegegeven.
$routeConfig = new RouteConfig('\Programmeren4\Article', $route, $appState);
$view = $routeConfig->invokeActionMethod();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MVC Web app</title>
    <link rel="stylesheet" href="css/article.css" type="text/css" />
</head>
<body>
    <?php
    $view();
    ?>
</body>
</html>