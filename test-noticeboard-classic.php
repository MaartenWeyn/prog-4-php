<?php
    include('vendor/modernways/dialog/src/Model/INotice.php');
    include('vendor/modernways/dialog/src/Model/Notice.php');
    include('vendor/modernways/dialog/src/Model/INoticeBoard.php');
    include('vendor/modernways/dialog/src/Model/NoticeBoard.php');
    
    $nb = new \ModernWays\Dialog\Model\NoticeBoard();
    $nb->startTimeInKey('test Dialog');
    $nb->setText('Mijn eerste foutmelding');
    $nb->setCaption('Dialog component testen');
    $nb->setCode('001');
    $nb->log();
    $nb->startTimeInKey('test Dialog 2');
    $nb->setText('Mijn tweede foutmelding');
    $nb->setCaption('Dialog component testen');
    $nb->setCode('002');
    $nb->log();
    
?><pre><?php //print_r($nb); ?></pre><?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>De Dialog component</title>
</head>
<body>
    
    <?php 
        $model = $nb;
        include('vendor/modernways/dialog/src/View/NoticeBoard.php');
    ?>
</body>
</html>