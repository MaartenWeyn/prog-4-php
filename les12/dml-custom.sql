use jefinghelbrecht;
DROP PROCEDURE IF EXISTS ArticleSelectAll;
DELIMITER //
CREATE PROCEDURE ArticleSelectAll()
BEGIN
SELECT `Article`.`Id`,
	`Article`.`Name`,
	`Article`.`PurchaseDate`,
	`Article`.`Price`
	FROM `Article`
	ORDER BY `Name`;
END //
DELIMITER ;

-- Stored Procedure
-- Article
-- Select row based on Id

use jefinghelbrecht;
DROP PROCEDURE IF EXISTS ArticleSelectOne;
DELIMITER //
CREATE PROCEDURE ArticleSelectOne(
	pId int
)
BEGIN
SELECT `Article`.`Id`,
	`Article`.`Name`,
	`Article`.`PurchaseDate`,
	`Article`.`Price`
	FROM `Article`
	WHERE pId = `Article`.`Id`
	ORDER BY `Name`;
END //
DELIMITER ;

use jefinghelbrecht;
DROP PROCEDURE IF EXISTS ArticleInsert;
DELIMITER //
CREATE PROCEDURE `ArticleInsert`
(
	IN pName VARCHAR (20) CHARACTER SET utf8,
	IN pPurchaseDate DATETIME,
    IN pPrice DECIMAL(6, 2),
	OUT pId INT 
)
BEGIN
INSERT INTO `Article`
	(
		`Article`.`Name`,
		`Article`.`PurchaseDate`,
		`Article`.`Price`       
	)
	VALUES
	(
		pName,
		pPurchaseDate,
        pPrice
	);
	SELECT LAST_INSERT_ID() INTO pId;
END //
DELIMITER ;

use jefinghelbrecht;
call ArticleInsert('Mercedes', '2016-12-15 12:37:30', '40.50', @Id);
select @Id;

use jefinghelbrecht;
DROP PROCEDURE IF EXISTS ArticleUpdate;
DELIMITER //
CREATE PROCEDURE `ArticleUpdate`
(
	IN pName VARCHAR (20) CHARACTER SET utf8,
	IN pPurchaseDate DATETIME,
    IN pPrice DECIMAL(6, 2),
	IN pId INT 
)
BEGIN
UPDATE `Article`
	SET `Article`.`Name` = pName,
		`Article`.`PurchaseDate` = pPurchaseDate,
		`Article`.`Price` = pPrice
WHERE `Article`.`Id` = pId;
END //
DELIMITER ;

use jefinghelbrecht;
call ArticleUpdate('Jaguar', '2015-12-19', 5.20, 6);

use jefinghelbrecht;
DROP PROCEDURE IF EXISTS ArticleDelete;
DELIMITER //
CREATE PROCEDURE ArticleDelete (
    IN pId INT
    )
BEGIN
	DELETE FROM `Article` 
    WHERE `Article`.`Id` = pId;
END //
DELIMITER ;

-- 6 moet wel een bestaande id zijn!!!!
call ArticleDelete(6);
