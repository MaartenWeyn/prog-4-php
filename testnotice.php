<?php 
include('vendor/modernways/dialog/src/Model/INotice.php');
include('vendor/modernways/dialog/src/Model/Notice.php');
include('vendor/modernways/dialog/src/Model/INoticeBoard.php');
include('vendor/modernways/dialog/src/Model/NoticeBoard.php');

$nb = new \modernways\Dialog\Model\NoticeBoard();
$nb->startTimeInKey('test Dialog');
$nb->setText('Mijn eerst foutmelding');
$nb->setCaption('Dialog component testen');
$nb->setCode('001');
$nb->log();
$nb->startTimeInKey('test Dialog 2');
$nb->setText('Mijn 2de foutmelding');
$nb->setCaption('Dialog component testen');
$nb->setCode('002');
$nb->log();


var_dump($nb);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Component</title>
    <style>
h1 {
    color: black;
    text-align: left;
    font-size: 40px;
}

p {
    font-family: verdana;
    font-size: 20px;
}
    </style>
</head>
<body>
    <pre>
        <?php var_dump($nb);?></pre>
        
        <?php 
        $model = $nb;
        include('vendor/modernways/dialog/src/View/NoticeBoard.php');
        ?>
        
</body>
</html>p