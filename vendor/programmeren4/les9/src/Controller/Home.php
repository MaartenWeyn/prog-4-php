<?php
namespace Programmeren4\Les9\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        //$model = 'Dat is 3 penny MVC';
        return $this->view('Home','Index', null);
    }
}