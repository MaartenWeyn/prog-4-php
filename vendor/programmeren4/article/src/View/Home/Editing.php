<main>
    <header>
        <h1>Winkel Maarten</h1>
    </header>
    <article>
        <nav>
            <a href="/article.php/Home/Inserting">Insert</a>
        </nav>
        <section>
            <?php
            if(count($model->getList()) > 0) {
                ?>
                <table>
                    <tr>
                        <th>Id</th>
                        <th>Naam</th>
                        <th>Purchase Date</th>
                        <th>Prijs</th>
                        <th></th>
                    </tr>
                    <?php
                    foreach ($model->getList() as $value) {
                        ?>
                        <tr>
                            <td><?php echo $value['Id']; ?></td>
                            <td><?php echo $value['Name']; ?></td>
                            <td><?php echo $value['PurchaseDate']; ?></td>
                            <td><?php echo $value['Price']; ?></td>
                            <td><a href="/article.php/Home/Updating/<?php echo $value['Id']; ?>">Update</a> | <a href="/article.php/Home/Delete/<?php echo $value['Id']; ?>">Delete</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <?php
            }else{
                ?>
                <p>Geen wagens gevonden</p>
                <?php
            }
            ?>
        </section>
    </article>
    <footer></footer>
</main>
<?php
$appStateView();
?>
<?php var_dump($model->getModelState());?>
