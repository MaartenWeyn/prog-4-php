<?php

// Dit is misschien niet de beste en snelste manier om foutboodschappen weer te geven maar ze werkt wel
$ArticleNameError = $ArticlePurchaseDateError = $ArticlePriceError = '';
if(isset($model) && $model->getModelState() != null) {
    foreach ($model->getModelState()->getBoard() as $value) {
        if($value->getCaption() == 'ArticleName')
            $ArticleNameError = $value->getText();
            
        if($value->getCaption() == 'ArticlePurchaseDate')
            $ArticlePurchaseDateError = $value->getText();
            
        if($value->getCaption() == 'ArticlePrice')
            $ArticlePriceError = $value->getText();
    }
}
?>
<main>
    <header>
        <h1>Garage Weyn</h1>
    </header>
    <article>
        <nav>
            <a href="/article.php/Home/Editing">Overzicht</a>
            
        </nav>
        <section>
            <form action="/article.php/Home/Insert" method="post">
                <div>
                    <label for="ArticleName">Naam</label>
                    <input type="text" name="ArticleName" id="ArticleName" value="<?php echo isset($model) && !empty($model->getName()) ? $model->getName() : '' ?>"/>
                    <span><?php echo $ArticleNameError; ?></span>
                </div>
                <div>
                    <label for="ArticlePurchaseDate">Aankoopdatum</label>
                    <input type="date" name="ArticlePurchaseDate" id="ArticlePurchaseDate" value="<?php if(isset($model) && !empty($model->getPurchaseDate())) { echo date('Y-m-d', strtotime($model->getPurchaseDate())); } ?>"/>
                    <span><?php echo $ArticlePurchaseDateError; ?></span>
                </div>
                <div>
                    <label for="ArticlePrice">Prijs</label>
                    <input type="text" name="ArticlePrice" id="ArticlePrice" value="<?php echo isset($model) && !empty($model->getPrice()) ? $model->getPrice() : null ?>"/>
                    <span><?php echo $ArticlePriceError; ?></span>
                </div>
                
                <button type="submit">Bewaar</button>
            </form>
        </section>
    </article>
    <footer></footer>
</main>
<?php
$appStateView();
?>

