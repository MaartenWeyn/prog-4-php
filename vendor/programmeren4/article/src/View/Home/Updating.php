<?php

// Dit is misschien niet de beste en snelste manier om foutboodschappen weer te geven maar ze werkt wel
$ArticleNameError = $ArticlePurchaseDateError = $ArticlePriceError = '';
if(isset($model) && $model->getModelState() != null) {
    foreach ($model->getModelState()->getBoard() as $value) {
        if($value->getCaption() == 'ArticleName')
            $ArticleNameError = $value->getText();
            
        if($value->getCaption() == 'ArticlePurchaseDate')
            $ArticlePurchaseDateError = $value->getText();
            
        if($value->getCaption() == 'ArticlePrice')
            $ArticlePriceError = $value->getText();
    }
}
?>
<main>
    <header>
        <h1>Garage Van Dooren</h1>
    </header>
    <article>
        <nav>
            <a href="/article.php/Home/Editing">Overzicht</a>
            <a href="">Zoeken</a>
        </nav>
        <section>
            <form action="/article.php/Home/Update" method="post">
                <input type="hidden" name="ArticleId" id="ArticleId" value="<?php echo $model->getId() ?>"/>
                <div>
                    <label for="ArticleName">Naam</label>
                    <input type="text" name="ArticleName" id="ArticleName" value="<?php echo $model->getName() ?>"/>
                    <span><?php echo $ArticleNameError; ?></span>
                </div>
                <div>
                    <label for="ArticlePurchaseDate">Aankoopdatum</label>
                    <input type="date" name="ArticlePurchaseDate" id="ArticlePurchaseDate" value="<?php echo date('Y-m-d', strtotime($model->getPurchaseDate())) ?>"/>
                    <span><?php echo $ArticlePurchaseDateError; ?></span>
                </div>
                <div>
                    <label for="ArticlePrice">Prijs</label>
                    <input type="text" name="ArticlePrice" id="ArticlePrice" value="<?php echo $model->getPrice() ?>"/>
                    <span><?php echo $ArticlePriceError; ?></span>
                </div>
                
                <button type="submit">Bewaar</button>
            </form>
        </section>
    </article>
    <footer></footer>
</main>
<?php $appStateView(); ?>

<pre><?php print_r($model->getModelState()->getBoard()); ?></pre>