<?php
namespace Programmeren4\Article\Model;

class Article extends \ModernWays\Mvc\Model
{
    private $id;
    private $name;
    private $purchasedDate;
    private $price;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
     public function getName()
    {
        return $this->name;
    }
    
    public function getNameUperCase() 
    {
        return strtoupper($this->name);
    }
    public function setName($name)
    {
        if (strlen($name) == 0)
        {
          $this->modelState->startTimeInKey('ArticleName');
            $this->modelState->setText('Naam mag niet leeg zijn!');
            $this->modelState->setCaption('ArticleName');
            $this->modelState->setCode('007');
            $this->modelState->log();
        }
        else
        {
            if (preg_match("/^[a-z0-9A-Z -]*$/",$name))
            {
                $this->name = $name;
            }
            else
            {
                $this->modelState->startTimeInKey('ArticleName');
                $this->modelState->setText('Alleen letters en cijfers!');
                $this->modelState->setCaption('ArticleName');
                $this->modelState->setCode('007');
                $this->modelState->log();
            }
        }
    }
    
  /**  public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }
  public function setPurchaseDate($purchaseDate) {
        $this->purchaseDate = $purchaseDate;
        if (strlen($purchaseDate) == 0) {
            $this->modelState->startTimeInKey('ArticlePurchaseDate');
            $this->modelState->setText('Aankoopdatum mag niet leeg zijn!');
            $this->modelState->setCaption('ArticlePurchaseDate');
            $this->modelState->setCode('007');
            $this->modelState->log();
        }else{
            list($date, $time) = explode(' ', $purchaseDate);
            $d = \DateTime::createFromFormat('Y-m-d', $date);
            if ($d && $d->format('Y-m-d') === $date) {
                 $this->purchaseDate = $purchaseDate;
            } else {
                $this->modelState->startTimeInKey('ArticlePurchaseDate');
                $this->modelState->setText('Aankoopdatum heeft geen geldige formaat!');
                $this->modelState->setCaption('ArticlePurchaseDate');
                $this->modelState->setCode('007');
                $this->modelState->log();
            }
        }
    }*/
    
     public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @param mixed $purchaseDate
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    }  
    
    public function getPrice() {
        return $this->price;
    }
    
    public function getVATIncluded() {
        return $this->price * 1.21;
    }
    
    public function setPrice($price) {
        if (strlen($price) == 0) {
            $this->modelState->startTimeInKey('ArticlePrice');
            $this->modelState->setText('Prijs mag niet leeg zijn!');
            $this->modelState->setCaption('ArticlePrice');
            $this->modelState->setCode('007');
            $this->modelState->log();
        }else{
            if (preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$price)) {
                 $this->price = $price;
            } else {
                $this->modelState->startTimeInKey('ArticlePrice');
                $this->modelState->setText('Enkel getallen met maximaal 2 cijfers na de comma zijn toegelaten!');
                $this->modelState->setCaption('ArticlePrice');
                $this->modelState->setCode('007');
                $this->modelState->log();
            }
        }
    }
}