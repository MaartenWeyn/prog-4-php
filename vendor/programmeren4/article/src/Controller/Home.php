<?php
namespace Programmeren4\Article\Controller;
/**
 * Class: HomeController
 * Programmeren 4
 */
class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;
    
    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null){
        parent::__construct($route,$noticeBoard);
        
 
        
         $this->noticeBoard->startTimeInKey('PDO Connection');
         try {
            $this->pdo = new \PDO('mysql:host=localhost;dbname=MaartenWeyn;charset=utf8', 'maartenweyn', '');
            
            $this->noticeBoard->setText("PDO Connection gelukt!");
            $this->noticeBoard->setCaption('PDO connectie voor Article2');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} in bestand {$e->getFile()}");
            $this->noticeBoard->setCaption('PDO connectie voor Article');
            $this->noticeBoard->setCode($e->getCode());
            
        }
        $this->noticeBoard->log();
        
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Programmeren4\Article\Model\Article($modelState);
    }
    
    public function editing() {
        if($this->pdo) {
            $command = $this->pdo->query("CALL ArticleSelectAll");
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            
            
            /**
             * Alternatief voor de bovenstaande code om gegevens uit de database te halen kan je onderstaande code
             * gebruiken. Bij bovenstaande code haal je de gegevens op als een array en zet je ze in een lijst.
             * Bij onderstaande code zal een instantie van het model worden aangemaakt en gevuld tijdens het ophalen.
             * Deze worden aan een lijst toegevoegd. Je zal een lijst van objecten doorsturen ipv een lijst van arrays.
             * De enige voorwaarden is dat de velden van het model dezelfde naam hebben als de kolomnamen in de
             * databank. Denk hierbij ook aan de pascal notatie.
             */
            // $sth = $this->pdo->query("CALL ArticleSelectAll");
            // $this->model->setList($sth->fetchAll(\PDO::FETCH_CLASS, "\Programmeren4\Article\Model\Article"));
        }
        return $this->view('Home', 'Editing', $this->model);
    }
     public function inserting() {
        return $this->view('Home', 'Inserting');
        
    }
    
    public function insert() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error', null);
        }
        
        if($this->model->isValid()) {
            $this->model->setName(filter_input(INPUT_POST, 'ArticleName', FILTER_SANITIZE_STRING));
            $this->model->setPurchaseDate(filter_input(INPUT_POST, 'ArticlePurchaseDate', FILTER_SANITIZE_STRING));
            $this->model->setPrice(filter_input(INPUT_POST, 'ArticlePrice', FILTER_SANITIZE_STRING));
        
            $sth = $this->pdo->prepare("CALL ArticleInsert(:pName, :pDate, :pPrice, @pId)");
            $sth->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $sth->bindValue(':pDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $sth->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_INT);
            $sth->execute();
            $this->model->setId($this->pdo->query('SELECT @pId')->fetchColumn());
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             * Als ik nu mijn pagina herlaad ga ik geen dubbele inserts krijgen.
             */
            header("Location: /article.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Inserting', $this->model);
        }
    }
        public function Updating() {
        if($this->pdo) {
            $this->model->setId($this->route->getId());
            $sth = $this->pdo->prepare("CALL ArticleSelectOne(:pId)");
            $sth->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $sth->execute();
            $array = $sth->fetch(\PDO::FETCH_ASSOC);
            
            $this->model->setName($array['Name']);
            $this->model->setPurchaseDate($array['PurchaseDate']);
            $this->model->setPrice($array['Price']);
            
            
            /**
             * Alternatief voor de bovenstaande code om gegevens uit de database te halen kan je onderstaande code
             * gebruiken. Bij bovenstaande code haal je de gegevens op en vul je nadien een instantie van het model.
             * Bij onderstaande code zal een instantie van het model worden aangemaakt en gevuld tijdens het ophalen.
             * De enige voorwaarden is dat de velden van het model dezelfde naam hebben als de kolomnamen in de
             * databank. Denk hierbij ook aan de pascal notatie.
             */
            // $this->model->setId($this->route->getId());
            // $sth = $this->pdo->prepare("CALL ArticleSelectOne(:pId)");
            // $sth->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            // $sth->setFetchMode(\PDO::FETCH_CLASS, "\Programmeren4\Article\Model\Article");
            // $sth->execute();
            // $this->model = $sth->fetch();
            
            
            return $this->view('Home', 'Updating', $this->model);
        } else {
            return $this->view('Home', 'Error');
        }
    }
    
    public function update() {
        if(!$this->pdo) {
            return $this->view('Home', 'Error');
        }
        $this->model->setName(filter_input(INPUT_POST, 'ArticleName', FILTER_SANITIZE_STRING));
        $this->model->setPurchaseDate(filter_input(INPUT_POST, 'ArticlePurchaseDate', FILTER_SANITIZE_STRING));
        $this->model->setPrice(filter_input(INPUT_POST, 'ArticlePrice', FILTER_SANITIZE_STRING));
        $this->model->setId(filter_input(INPUT_POST, 'ArticleId', FILTER_SANITIZE_NUMBER_INT));
        
        if($this->model->isValid()) {
            $sth = $this->pdo->prepare("CALL ArticleUpdate(:pName, :pDate, :pPrice, :pId)");
            $sth->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $sth->bindValue(':pDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $sth->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_INT);
            $sth->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $sth->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            header("Location: /article.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Updating', $this->model);
        }
    }
    
    public function delete() {
        if($this->pdo) {
            $sth = $this->pdo->prepare("CALL ArticleDelete(:pId)");
            $sth->bindValue(':pId', $this->route->getId(), \PDO::PARAM_INT);
            $sth->execute();
            
            /**
             * Ik gebruik hier header() omdat ik niet alleen de editing pagina te zien wil krijgen,
             * ik wil effectief doorgestuurd worden naar deze pagina. 
             */
            header("Location: /article.php/Home/Editing");
            //return $this->editing();
        } else {
            return $this->view('Home', 'Error');
        }
    
    }
}