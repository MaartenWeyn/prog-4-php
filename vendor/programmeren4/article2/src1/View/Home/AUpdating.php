<main>
    <header>
        <h1>Garage - Programmeren 4</h1>
    </header>
    <article>
        <nav>
            <a href=""></a>
            <a href=""></a>
        </nav>
        <form action="/article.php/Home/update" method="post">
            <div>
                <label for="ArticleId">Id:</label>
                <input type="text" name="ArticleId" value="<?php echo $model->getId();?>"/>
            </div>
            <div>
                <label for="ArticleName">Name:</label>
                <input type="text" name="ArticleName" value="<?php echo $model->getName();?>"/>
            </div>
            <div>
                <label for="ArticlePurchaseDate">Purchase Date:</label>
                <input type="datetime-local" name="ArticlePurchaseDate" value="<?php echo $model->getPurchaseDate();?>"/>
            </div>
            <div>
                <label for="ArticlePrice">Price:</label>
                <input type="text" name="ArticlePrice" value="<?php echo $model->getPrice();?>"/>
            </div>
            <br />
            <button id="submit">Update</button>
        </form>
        
    </article>
</main>
<?php $appStateView(); ?>