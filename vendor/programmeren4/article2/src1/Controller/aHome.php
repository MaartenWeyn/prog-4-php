<?php
namespace Programmeren4\Article\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    private $pdo;
    private $model;

    public function __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null)
    {
        parent::__construct($route, $noticeBoard);
        
        $this->noticeBoard->startTimeInKey('PDO Connection');

        try {
            $this->pdo = new \PDO('mysql:host=localhost;dbname=MaartenWeyn;charset=utf8', 'maartenweyn', '');
            
            $this->noticeBoard->setText("PDO connectie gelukt.");
            $this->noticeBoard->setCaption('PDO connectie voor Article.');
        } catch (\Exception $e) {
            $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} : {$e->getFile()}.");
            $this->noticeBoard->setCaption('PDO connectie voor Article.');
            $this->noticeBoard->setCode($e->getCode());
        }
        $this->noticeBoard->log();
        
        //create een instantie van een prikbord om validatiefouten te plaatsen.
        $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
        $this->model = new \Programmeren4\Article\Model\Article($modelState);
    }
    
    public function editing()
    {
        if ($this->pdo) {
            // het model vullen
            $command = $this->pdo->query("call ArticleSelectAll");
            // associatieve array kolomnamen en waarde per rij
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            return $this->view('Home','Editing', $this->model);
        }
        else {
            $this->view('Home', 'Error', null);
        }
    }
    
    public function inserting()
    {
        return $this->view('Home', 'Inserting', null);
    }
    
    public function insert()
    {
        if (!$this->pdo) {
            return $this->view('Home', 'Error', null);
        }
        if ($this->model->isValid()) {
            $this->model->setName(filter_input(INPUT_POST, 'ArticleName', FILTER_SANITIZE_STRING));
            $this->model->setPurchaseDate(filter_input(INPUT_POST, 'ArticlePurchaseDate', FILTER_SANITIZE_STRING));
            $this->model->setPrice(filter_input(INPUT_POST, 'ArticlePrice', FILTER_SANITIZE_STRING));
            $statement = $this->pdo->prepare("call ArticleInsert(:pName, 
                :pPurchaseDate, :pPrice, @pId)");
            // bindValue is by reference
            // bindParam is by value
            $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
            $statement->bindValue(':pPurchaseDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
            $statement->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_STR);
            $result = $statement->execute();
            // zet de nieuw toegekende Id in het Id veld van het model
            // $this->model->setId($pdo->query('select @pId')->fetchColumn());
            return $this->editing();
        } else {
            return $this->view('Home', 'Inserting', $model);
        }
    }
    
    public function updating()
    {
        if($this->pdo) {
            $this->model->setId($this->route->getId());
            $statement = $this->pdo->prepare("call ArticleSelectOne(:pId)");
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $result = $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setPurchaseDate($articleOne['PurchaseDate']);
            $this->model->setPrice($articleOne['Price']);
            return $this->view('Home', 'Updating', $this->model);
        }
        else {
            return $this->view('Home', 'Error', null);
        }
        
    }
    
    public function delete(){
        
    }
}