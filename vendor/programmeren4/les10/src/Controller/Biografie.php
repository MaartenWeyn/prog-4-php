<?php
namespace Programmeren4\Les10\Controller;

class Biografie extends \ModernWays\Mvc\Controller
{
   public function showBiografie()
   {
        $model = new \Programmeren4\Les10\Model\Biografie();
        return $this->view('Biografie','ShowBiografie', $model);
   }
}