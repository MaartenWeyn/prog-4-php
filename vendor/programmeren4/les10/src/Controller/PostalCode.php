<?php
namespace Programmeren4\Les10\Controller;

class PostalCode extends \ModernWays\Mvc\Controller
{
    public function show()
    {
        $textPostcalCodes = file_get_contents('data/Postcodes.csv');
        $lines = explode("\n", $textPostcalCodes);
        $model = new \Programmeren4\Les10\Model\PostalCode();
        $postalCodes = array();
        foreach ($lines as $line) {
            $postalCodes[] =  explode('|', $line);
        }
        // var_dump($postalCodes);
        $model->setList($postalCodes);
        return $this->view('PostalCode','Show', $model);
    }
}