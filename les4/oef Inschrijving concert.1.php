<?php
    $voornaamError = '';
    $naamError = '';
    $plaatsError = '';
    $aantalpersonenError ='';
    $geboortedatumError ='';
    $rknnummerError ='';
    $emailError=''; 
    $voornaam=$naam=$aantalpersonen=$geboortedatum=$rknnummer=$email="";
    if($SERVER["REQUEST_METHOD"] == POST){
    if (isset($_Post['voornaam']))  {
        $voornaamNaam = $_POST['voornaam'];
        if (empty($voornaamNaam)){
            $voornaamError = 'Voornaam moet een naam hebben!';
        } else
        {
            $naamPattern = '/^[a-zA-Zéç ]*$/';
            if (!preg_match($voornaamPattern, $voornaamNaam)) {
                $naamPattrnError = 'Naam bestaat uit karakters alleen!';
            }
        }
    }
    if (isset($_Post['naam']))  {
        $naamNaam = $_POST['naam'];
        if (empty($naamNaam)){
            $naamError = 'Familienaam moet een naam hebben!';
        } else
        {
            $naamPattern = '/^[a-zA-Zéç ]*$/';
            if (!preg_match($voornaamPattern, $voornaamNaam)) {
                $naamPattrnError = 'Naam bestaat uit karakters alleen!';
            }
        }
    }
    if (isset($_POST['aantalpersonen'])) {
        $aantalpersonen = $_POST['aantalpersonen'];
        if (empty($aantalpersonen)) {
            $aantalpersonenError = 'Er moet een getal opgegeven worden!';
        } else {    
            $aantalPattern = '/^[0-9]?$/';
            if (!preg_match($aantalPattern, $aantalpersonen)) {
                $aantalError = 'Typ een geldig getal in! Bv. 5!';
            }
        }
        
    }
     if (isset($_POST['rknnummmer'])) {
        $rknnummer = $_POST['rknnummer'];
        if (empty($rknnummer)) {
            $rknnummerError = 'Er moet een rekening nummer opgegeven worden!';
        } else {    
           if (!preg_match($rknnummerPattern, $rknnummer)) {
             $rknnummerError = "Een geldig rekeningnumme opgeven!"; 
            }
        }
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
        if (empty($email)) {
            $emailError = 'Er moet een email-adres opgegeven worden!';
        } else {    
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             $emailError = "Invalid email format!"; 
            }
        }
    }
    }
    
    
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inschrijving concert</title>
    
</head>
<body>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post">
        <h1>Bob Dylan</h1>
        
        <h2>Inschrijvingsformulier</h2>
        <label for="voornaam">Voornaam</label>
        <input id="voornaam" name="voornaam" required type="text" pattern="^[a-zA-Z]+$" >
        <span><?php echo $voornaamError;?></span>
        <br>
        
        <label for="naam">Naam</label>
        <input id="naam" name="naam" required type="text" pattern="^[a-zA-Z]+$" >
        <span><?php echo $naamError;?></span>
        <br>
        
        <label for="plaats">Plaats </label>
                <select name="plaats" id="plaats">
                    <option value="0">Staanplaats</option>
                    <option value="1">Tribune</option>
                    <option value="2">Balkon</option>
                </select>
            
        <br>
        
        <label for="aantalpersonen">Aantal personen</label>
        <input id="aantalpersonen" name="aantalpersonen" type="text" required  pattern="^[0-9]?$">
        <span><?php echo $aantalpersonenError;?></span>
        <br>
        <label for="geboortedatum">Geboortedatum</label>
        <input id="geboortedatum" name="geboortedatum" type="date">
        <span><?php echo $geboortedatumError;?></span>
        <br>
        <label for="rknnummer">Rekeningnummer</label>
        <input id="rknnummer" name="rknnummer" type="text" equired  pattern="" placeholder="BE23000045627382">
         <span><?php echo $rknnummerError;?></span>
        <br>
        <label for="email">Emailadres</label>
        <input id="email" name="email" type="text"  pattern="" placeholder="Voorbeeld@gmail.com">
        <span><?php echo $emailError;?></span>
        <br>
        <input type="submit" value="Submit"/>
    </form>
</body>
</html>