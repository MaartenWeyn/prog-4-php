<?php
    $persons = array (
        array('Johanna', 'Den Doper', 117, 'Vrouw'),
        array('Mohamed', 'El Farisi', 40, 'Man'),
        array('Johanna', 'Den Doper', 117, 'Vrouw'),
        array('Mohamed', 'El Farisi', 40, 'Man'),
        array('Johanna', 'Den Doper', 117, 'Vrouw'),
        array('Mohamed', 'El Farisi', 40, 'Man'),
    );
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Werken met arrays in PHP</title>
</head>
<body>
    <table>
        <thead>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($persons as $person) { ?>
                <tr>
                    <td><?php echo $person[0];?></td>
                    <td><?php echo $person[1];?></td>
                    <td><?php echo $person[2];?></td>
                    <td><?php echo $person[3];?></td>
                </tr>
            <?php 
            } ?>
            </tbody>
    </table>
</body>
</html>