<?php
$textPostalCodes = file_get_contents("../data/Postcodes.csv");
$postalCodes = explode("\n",$textPostalCodes);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>De BehandelRegistratie pagina</title>
    <style>
        th, td {
   border: 1px solid black;
}
    </style>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Postcode</th>
                <th>Stad</th>
                <th>Code Postal</th>
                <th>Ville</th>
            </tr>
        </thead>
        <tbody>
    <?php
    foreach ($postalCodes as $row) {
        $postalCode = explode('|', $row);
        ?>
        <tr>
            <td><?php echo $postalCode[0];?></td>
            <td><?php echo $postalCode[1];?></td>
            <td><?php echo $postalCode[2];?></td>
            <td><?php echo $postalCode[3];?></td>
        </tr>
    <?php 
    } ?>
    </tbody>
    </table>    
</body>
</body>
</html>