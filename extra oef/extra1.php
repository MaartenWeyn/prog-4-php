<body>
    <h1>Formulier voorbeeld eerste versie</h1>
    <p><span class="error">* verplicht veld</span></p>
    <form method="post" action="#">
        <fieldset>
            <div>
                <label for="name">Naam</label>
                <input type="text" name="name" autofocus required>
                <span class="error">*</span>
            </div>
                      <div>
                <label for="email">E-mail</label>
                <input type="email" name="email" required placeholder="user@provider.domain">
                <span class="error">*</span>
            </div>
            <div>
                <label for="website">Website</label>
                <input type="url" name="website" placeholder="www.provider.name">
            </div>
            <div>
                <label for="comment">Commentaar</label>
                <textarea name="comment" rows="5" cols="40"></textarea>
            </div>
            <div>
                <span>Hoe ben je op onze site terechtgekomen?</span>
                <input name="refer-random" type="checkbox" id="refer_random" value="random" />
                <label for="refer-random">Toevallig</label>
                <input name="refer-friend" type="checkbox" id="refer-friend" value="friend" />
                <label for="refer-friend">Vriend</label>
                <input name="refer-search-engine" type="checkbox" id="refer-search-engine" value="search-engine" />
                <label for="refer-search-engine">Zoekmachine</label>
           </div>
            <div>
                <label for="rating">Wat vind je van deze site?</label>
                <input type="radio" name="rating" id="rate-5" value="5"/><label for="rate-5">Schitterend</label>
                <input type="radio" name="rating" id="rate-4" value="4"><label for="rate-4">Goed</label>
                <input type="radio" name="rating" id="rate-3" value="3"><label for="rate-4">Mmmhh</label>
                <input type="radio" name="rating" id="rate-2" value="2"><label for="rate-4">Slecht</label>
                <input type="radio" name="rating" id="rate-1" value="1"><label for="rate-4">Heel slecht</label>
                <span class="error">* <?php echo $ratingErr;?></span>
            </div>
            <div>
                <label for="country">Land</label>
                <select name=country>
                    <option value="BE">België</option>
                    <option value="DE">Duitsland</option>
                    <option value="FR">Frankrijk</option>
                    <option value="LU">Luxemburg</option>
                    <option value="NL">Nederland</option>
                    <option value="UK">Verenigd Koninkrijk</option>
                </select>
            </div>
        </fieldset>
        <div class="commandbar">
            <button type="submit" name="uc" value="User-Insert">Insert</button>
        </div>
    </form>
</body>